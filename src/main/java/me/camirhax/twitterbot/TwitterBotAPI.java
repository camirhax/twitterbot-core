package me.camirhax.twitterbot;

import twitter4j.AccountSettings;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.io.*;
import java.util.Base64;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public abstract class TwitterBotAPI
{

    public static File CONFIGURATION = new File("twitterbot_settings.xml");


    private static final String API_KEY = "UsidBlAr34oHP3KxIqqmOlhRq";
    private static final char[] API_SECRET = new char[]{'\u003d','\u006b','\u0054','\u004d','\u006c','\u0056','\u006a','\u0054','\u0033','\u006c','\u0033','\u0052','\u0059','\u004e','\u0058','\u0054','\u0079','\u0034','\u0030','\u0054','\u0032','\u0070','\u0057','\u004d','\u0059','\u0064','\u0044','\u0056','\u006c','\u0078','\u006d','\u0051','\u006a','\u004e','\u0054','\u0065','\u0057','\u0068','\u0055','\u0059','\u0033','\u0059','\u0048','\u004d','\u0047','\u004a','\u006e','\u0052','\u006f','\u005a','\u0044','\u0062','\u0079','\u0068','\u0031','\u0059','\u0051','\u0070','\u0033','\u0059','\u0032','\u005a','\u006a','\u0063','\u0047','\u0064','\u0054','\u005a'};

    private static TwitterBotAPI instance;

    public static TwitterBotAPI getInstance()
    {
        return instance;
    }

    public Properties properties = new Properties();
    public Twitter twitter;

    public TwitterBotAPI()
    {
        if(instance != null)
            throw new IllegalStateException("TwitterBotAPI is already initialized in this runtime!");
        instance = this;
    }
    public TwitterBotAPI(File configuration)
    {
        this();
        TwitterBotAPI.CONFIGURATION = configuration;
    }

    public void handleLogin()
    {

        if(!loadProperties())
            return;

        twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(API_KEY, new String(decrypt(API_SECRET)));
        AccessToken token;
        if(!properties.containsKey("twitter.oauth.token") || !properties.containsKey("twitter.oauth.secret"))
        {
            // We need to get an Token from twitter...
            token = requestAccessToken();
            properties.setProperty("twitter.oauth.token", token.getToken());
            properties.setProperty("twitter.oauth.secret", new String(encrypt(token.getTokenSecret().toCharArray())));
            saveProperties();
        }
        else
        {
            token = new AccessToken(properties.getProperty("twitter.oauth.token"), new String(decrypt(properties.getProperty("twitter.oauth.secret").toCharArray())));
        }

        twitter.setOAuthAccessToken(token);

        try{
            AccountSettings settings = twitter.users().getAccountSettings();
            System.out.println("Logged in as " + settings.getScreenName() +" [" + twitter.lookupUsers(new String[]{settings.getScreenName()}).get(0).getId() + "]");
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public char[] encrypt(char[] base)
    {
            return reverse(new String(Base64.getEncoder().encode(new String(base).getBytes())).toCharArray());
    }

    public char[] decrypt(char[] base)
    {
        byte[] dec = Base64.getDecoder().decode(new String(reverse(base)).getBytes());
        return new String(dec).toCharArray();
    }

    private char[] reverse(char[] base)
    {
        char[] reverse = new char[base.length];
        int z = 0;
        for(int i = base.length-1; i >= 0; i--)
        {
            reverse[z] = base[i];
            z++;
        }
        return reverse;
    }

    public boolean loadProperties()
    {
        try{
            if(CONFIGURATION.exists())
                properties.loadFromXML(new FileInputStream(CONFIGURATION));

        }catch (Exception e)
        {
            System.err.println("Error while trying to load configuration file, please report this exception if you think it is a bug: " + e.getMessage());
            return false;
        }
        return true;
    }

    public void saveProperties()
    {
        try{
            if(!CONFIGURATION.exists())
                CONFIGURATION.createNewFile();

            properties.storeToXML(new FileOutputStream(CONFIGURATION), null);
        }catch (Exception e)
        {
            System.err.println("Error while trying to save configuration file, please report this exception if you think it is a bug: " + e.getMessage() + ". A solution to this could be to delete the config file '" + CONFIGURATION.getName() + "'");
        }
    }

    private AccessToken requestAccessToken()
    {
        final Storage<AccessToken> sAccessToken = new Storage<>();
        System.out.println("Twitter token is not set up yet or invalid. Requesting new one...");
        try{
            final RequestToken token = twitter.getOAuthRequestToken();
            final CountDownLatch latch = new CountDownLatch(1);

            new Thread(() -> getAccessTokenFromWeb(token, latch, sAccessToken)).start();

            latch.await();
        }catch (Exception e)
        {
            System.err.println("Error while trying to retrieve oAuth token, please report this exception if you think it is a bug: " + e.getMessage());
        }
        return sAccessToken.get();
    }

    private void getAccessTokenFromWeb(RequestToken token, CountDownLatch latch, Storage<AccessToken> sAccessToken)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(sAccessToken.get() == null)
        {
            System.out.println("Open the following URL and grant access to your account:");
            System.out.println(token.getAuthorizationURL());
            System.out.print("Enter the PIN(if available) and hit enter after you granted access.[PIN]:");
            String pin = "";
            try{
                pin = reader.readLine();
            }catch (Exception e){
                System.err.println("Invalid input! Please try again.");
            }

            try {
                if (pin.length() > 0) {
                    sAccessToken.set(twitter.getOAuthAccessToken(token, pin));
                } else {
                    sAccessToken.set(twitter.getOAuthAccessToken(token));
                }
            } catch (TwitterException te) {
                if (401 == te.getStatusCode()) {
                    System.out.println("Unable to get the access token. Please try again.");
                } else {
                    System.err.println("Error while trying to retrieve OAuth token, please report this exception if you think it is a bug and try again: " + te.getMessage());
                }
            }
        }
        latch.countDown();
        AccessToken at = sAccessToken.get();
        System.out.println("Successfully retrieved OAuth Token for user " + at.getScreenName() + "[" + at.getUserId() + "] from Twitter: [" + at.getToken() + " || " + at.getTokenSecret() + "]");
    }

    public class Storage<T>
    {
        private T obj;
        public Storage(){}

        public T get() {
            return obj;
        }

        public void set(T obj) {
            this.obj = obj;
        }
    }

    public abstract void stop();

}
