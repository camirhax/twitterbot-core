package me.camirhax.twitterbot.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Command
{

    private String name, description;
    private String[] alias = new String[0];


    public Command(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    public Command(String name)
    {
        this(name, null);
    }


    public void addAlias(String... alias)
    {
        this.alias = Arrays.asList(alias).toArray(alias);
    }


    public abstract void execute(String[] arguments);

    public String getName()
    {
        return this.name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String[] getAliases()
    {
        return this.alias;
    }

    public List<String> getListeningNames()
    {
        ArrayList<String> l = new ArrayList<>();

        l.add(name);
        if(alias != null && alias.length != 0)
            l.addAll(Arrays.asList(alias));

        return l;
    }


}
