package me.camirhax.twitterbot.command;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class CommandManager implements Runnable
{

    private static boolean listening = false;
    private static Thread listener;
    private static HashMap<String, Command> registeredCommands = new HashMap<>();
    private static ArrayList<Command> uniqueCommands = new ArrayList<>();

    static
    {
        InternalCommands.registerCommands();
    }

    public static void startListening()
    {
        listening = true;
        listener = new Thread(new CommandManager());
        listener.start();
        System.out.println("For help type \"help\" or \"?\"");
    }

    public static void stopListening()
    {
        listening = false;
        listener.interrupt();

    }

    public static void registerCommand(Command cmd)
    {

        for(String alias : cmd.getListeningNames())
        {
            if(registeredCommands.containsKey(alias.toLowerCase()))
            {
                // Error: Command already registered! We could log this to the console but it is kinda debug information
                continue;
            }

            registeredCommands.put(alias.toLowerCase(), cmd);
        }

        uniqueCommands.add(cmd);

        System.out.println("Successfully registered command " + cmd.getName());
    }

    public static void handleCommand(String input)
    {
        String[] args = input.split("\\s+");

        String name = args[0];

        args = Arrays.copyOfRange(args, 1, args.length);

        if(name.equalsIgnoreCase("help") || name.equalsIgnoreCase("?"))
        {

            printHelp(args);

        }else if(registeredCommands.containsKey(name.toLowerCase()))
        {
            Command cmd = registeredCommands.get(name.toLowerCase());
            cmd.execute(args);
        }
        else
        {
            System.out.println("Command not found! For a list of commands type \"help\" or \"?\"!");
        }

    }


    public static void printHelp(String[] args)
    {
        double pageSize = 5.0;
        int pages = (int) Math.ceil(uniqueCommands.size() / pageSize);
        int index = 1;

        if(pages > 1 && args.length > 0)
        {
            try{
                index = Integer.parseInt(args[0]);
            }catch (Exception e)
            {}
        }

        String head = " -------- Help (Page " + index + " of " + (pages == 0 ? 1 : pages) +") -------- ";
        String foot = " ";

        for(int i = 0; i < head.length()-2 ; i++)
            foot +="-";

        foot+=" ";

        System.out.println(head);

        if(pages == 0)
        {
            System.out.println(foot);
            return;
        }

        int start = (int)((index - 1) * pageSize);
        int end = (int)(index * pageSize);

        ArrayList<Command> shownCommands = new ArrayList<>();
        int largestName = 0;

        for(int i = start; i <= end; i++)
        {
            if(uniqueCommands.size() <= i)
                break;

            Command cmd = uniqueCommands.get(i);
            if(cmd.getName().length() > largestName)
                largestName = cmd.getName().length();
            shownCommands.add(cmd);
        }

        for(Command cmd : shownCommands)
        {
            String s = " | " + cmd.getName();

            int spaces = largestName - s.length()+2;

            for(int i = 0; i <= spaces; i++)
                s+=" ";

            s+=" |  " + (cmd.getDescription() != null ? cmd.getDescription() : "");
            System.out.println(s);
        }



        System.out.println(foot);




    }


    private CommandManager(){}

    @Override
    public void run()
    {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while(listening)
        {
            try{
                String line = reader.readLine();
                if(line.length() == 0)
                    continue;

                handleCommand(line);
            }catch (Exception e){}



        }

    }
}
