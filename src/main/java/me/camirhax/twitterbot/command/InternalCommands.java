package me.camirhax.twitterbot.command;

import me.camirhax.twitterbot.TwitterBotAPI;

public class InternalCommands
{

    protected static void registerCommands()
    {
        CommandManager.registerCommand(new CommandStop());
    }

    private static class CommandStop extends Command
    {
        public CommandStop()
        {
            super("stop", "Stops this Bot");
        }

        @Override
        public void execute(String[] arguments)
        {

            TwitterBotAPI.getInstance().stop();

        }
    }


}
